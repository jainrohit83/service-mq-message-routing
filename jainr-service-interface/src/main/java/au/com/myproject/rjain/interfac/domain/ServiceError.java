package au.com.myproject.rjain.interfac.domain;

public class ServiceError {

    private String code;

    private String text;

    public ServiceError() {
    }

    public ServiceError(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
