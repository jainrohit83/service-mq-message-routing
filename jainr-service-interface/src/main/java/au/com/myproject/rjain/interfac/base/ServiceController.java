package au.com.myproject.rjain.interfac.base;

import au.com.myproject.rjain.interfac.domain.ServiceError;
import au.com.myproject.rjain.interfac.domain.ServiceRequest;
import au.com.myproject.rjain.interfac.domain.ServiceResponse;
import au.com.myproject.rjain.interfac.ex.ServiceException;
import au.com.myproject.rjain.interfac.helper.JsonHelper;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ServiceController {

    private static final Logger log = Logger.getLogger(ServiceController.class);

    private Map<String, ProcessorData> resourceProcessorData = new HashMap<>();

    public ServiceController(List<Object> resourceProcessors) {
        resourceProcessorData = new ProcessorScanner(resourceProcessors).getResourceProcessorData();
    }

    public String handle(String request) {

        ServiceResponse serviceResponse;
        ServiceRequest serviceRequest = JsonHelper.fromJson(request, ServiceRequest.class);
        try {
            serviceResponse = handleOrError(serviceRequest);
        } catch (ServiceException ex) {
            log.error("Known error while handling request.", ex);
            serviceResponse = new ServiceResponse(serviceRequest);
            serviceResponse.setError(new ServiceError(ex.getCode(), ex.getText()));
        } catch (Exception ex) {
            log.error("UnKnown error while handling request.", ex);
            serviceResponse = new ServiceResponse(serviceRequest);
            serviceResponse.setError(new ServiceError("SE", getRootCause(ex).getMessage()));
        }
        return JsonHelper.toJson(serviceResponse);
    }

    public Throwable getRootCause(Throwable throwable) {

        if (throwable.getCause() != null) {
            return getRootCause(throwable.getCause());
        }
        return throwable;
    }

    private ServiceResponse handleOrError(ServiceRequest serviceRequest) throws Exception {

        ServiceResponse serviceResponse;
        if (isValidResourceAction(serviceRequest)) {
            serviceResponse = processResourceAction(serviceRequest);
        } else {
            throw new Exception("Invalid service operation.");
        }
        return serviceResponse;
    }

    private ServiceResponse processResourceAction(ServiceRequest serviceRequest) throws InvocationTargetException, IllegalAccessException {

        ProcessorData processorData = resourceProcessorData.get(serviceRequest.getResource().toLowerCase());
        Object processor = processorData.getResourceProcessor();
        String action = serviceRequest.getAction().toLowerCase();
        log.debug("fetching action : " + action);
        Method method = processorData.getMethod(serviceRequest.getAction().toLowerCase());
        log.debug("Service Request:" + JsonHelper.toJson(serviceRequest));
        return (ServiceResponse) method.invoke(processor, serviceRequest);
    }

    private boolean isValidResourceAction(ServiceRequest serviceRequest) {

        boolean isValidAction = false;
        String resource = serviceRequest.getResource().toLowerCase();
        String action = serviceRequest.getAction().toLowerCase();
        ProcessorData processorData = resourceProcessorData.get(resource);
        if (processorData != null && processorData.getActions().contains(action)) {
            isValidAction = true;
        }
        return isValidAction;
    }
}
