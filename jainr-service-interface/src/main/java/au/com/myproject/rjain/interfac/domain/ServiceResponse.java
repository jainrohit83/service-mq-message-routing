package au.com.myproject.rjain.interfac.domain;

import au.com.myproject.rjain.interfac.helper.JsonHelper;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;

public class ServiceResponse {

    private String msgId;
    private Object responseData;
    private ServiceError error;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public ServiceResponse(ServiceRequest serviceRequest) {
        this.msgId = serviceRequest.getMsgId();
    }

    @JsonRawValue
    public String getResponseData() {
        return responseData == null ? null : responseData.toString();
    }

    public <T> T getResponseData(Class<T> clazz) {
        return JsonHelper.fromJson(getResponseData(), clazz);
    }

    public <T> T getResponseData(JavaType type) {
        return JsonHelper.fromJson(getResponseData(), type);
    }

    public void setResponseData(JsonNode responseData) {
        this.responseData = responseData;
    }

    public void setResponseData(Object object) {
        if (object != null) {
            this.responseData = JsonHelper.toJson(object);
        }
    }

    public ServiceError getError() {
        return error;
    }

    public void setError(ServiceError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return JsonHelper.toJson(this);
    }
}
