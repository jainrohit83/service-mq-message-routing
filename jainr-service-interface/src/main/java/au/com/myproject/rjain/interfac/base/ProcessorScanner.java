package au.com.myproject.rjain.interfac.base;

import au.com.myproject.rjain.interfac.base.annotation.ActionProcessor;
import au.com.myproject.rjain.interfac.base.annotation.ResourceProcessor;
import org.apache.log4j.Logger;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessorScanner {

    private static final Logger log = Logger.getLogger(ProcessorScanner.class);

    private Map<String, ProcessorData> resourceProcessorData = new HashMap<>();

    public ProcessorScanner(List<Object> resourceProcessors) {

        init(resourceProcessors);
        log.info("################################################################################");
        log.info("---------------------REGISTERED RESOURCE PROCESSORS---------------------------");
        for (Map.Entry<String, ProcessorData> entry : resourceProcessorData.entrySet()) {
            String resource = entry.getKey();
            ProcessorData processorData = entry.getValue();
            log.info("Resource(" + resource + "):\t\t Class(" + processorData.getResourceProcessor().getClass().getName() + ")");
            for (Map.Entry<String, Method> actionMethod : processorData.getActionMethodMap().entrySet()) {
                log.info("\tAction(" + actionMethod.getKey() + "):\t\t Method(" + actionMethod.getValue().getName() + ")");
            }
        }
        log.info("################################################################################");
    }

    public Map<String, ProcessorData> getResourceProcessorData() {
        return resourceProcessorData;
    }

    private void init(List<Object> resourceProcessors) {

        for (Object resourceProcessor : resourceProcessors) {
            Class<?> clazz = getResourceClazz(resourceProcessor);

            ProcessorData processorData = new ProcessorData();
            processorData.setResourceProcessor(resourceProcessor);
            processorData.setActionMethodMap(getActionMethodMap(clazz));

            Map<String, Method> actionsMethodMap = getActionMethodMap(clazz);

            if (!actionsMethodMap.isEmpty()) {
                processorData.setActionMethodMap(actionsMethodMap);
                String resource = clazz.getAnnotation(ResourceProcessor.class).resource().toLowerCase();
                resourceProcessorData.put(resource, processorData);
            }
        }
    }

    private Class<?> getResourceClazz(Object resourceProcessor) {

        Class<?> clazz;
        if (AopUtils.isCglibProxy(resourceProcessor) || AopUtils.isJdkDynamicProxy(resourceProcessor)) {
            clazz = ((Advised)resourceProcessor).getTargetSource().getTargetClass();
        } else {
            clazz = resourceProcessor.getClass();
        }
        return clazz;
    }

    private Map<String, Method> getActionMethodMap(Class<?> clazz) {

        Map<String, Method> actionMethodMap = new HashMap<>();
        String resource = clazz.getAnnotation(ResourceProcessor.class).resource();
        if (!resource.isEmpty()) {
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                ActionProcessor actionProcessor = method.getAnnotation(ActionProcessor.class);
                if (actionProcessor != null && !actionProcessor.action().isEmpty()) {
                    actionMethodMap.put(actionProcessor.action().toLowerCase(), method);
                }
            }
        }
        return actionMethodMap;
    }
}
