package au.com.myproject.rjain.interfac.ex;

public class ServiceException extends RuntimeException {

    private String code;

    private String text;

    public ServiceException(String message, String code, String text) {
        super(message);
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
