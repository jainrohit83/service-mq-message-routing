package au.com.myproject.rjain.interfac.base;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

public final class ProcessorData {

    private Object resourceProcessor;

    private Map<String, Method> actionMethodMap;

    public Object getResourceProcessor() {
        return resourceProcessor;
    }

    public void setResourceProcessor(Object resourceProcessor) {
        this.resourceProcessor = resourceProcessor;
    }

    public Map<String, Method> getActionMethodMap() {
        return actionMethodMap;
    }

    public void setActionMethodMap(Map<String, Method> actionMethodMap) {
        this.actionMethodMap = actionMethodMap;
    }

    public Set<String> getActions() {
        return actionMethodMap.keySet();
    }

    public Method getMethod(String action) {
        return actionMethodMap.get(action);
    }
}
