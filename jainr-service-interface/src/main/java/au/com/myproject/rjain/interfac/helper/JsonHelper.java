package au.com.myproject.rjain.interfac.helper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JsonHelper {

    private static final Logger log = LoggerFactory.getLogger(JsonHelper.class);

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        SimpleModule simpleModule = new SimpleModule();
        mapper.registerModule(simpleModule);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {

        T object = null;
        try {
            object = mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.warn("Error while reading object as json:" + json, e);
        }
        return object;
    }

    public static <T> T fromStream(String name, Class<T> clazz) {

        InputStream stream = JsonHelper.class.getClassLoader().getSystemResourceAsStream(name);
        T object = null;
        try {
            object = mapper.readValue(stream, clazz);

        } catch (Exception e) {
            log.warn("Error while reading object as json from stream:" + name, e);
        }
        return object;
    }

    public static <T> List<T> fromStreamtoList(String name, Class<T> clazz) {

        InputStream stream = JsonHelper.class.getClassLoader().getSystemResourceAsStream(name);
        List<T> list = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            TypeFactory t = TypeFactory.defaultInstance();
            list = mapper.readValue(stream, t.constructCollectionType(ArrayList.class, clazz));
        } catch (Exception e) {
            log.warn("Error while reading object as json from stream:" + name, e);
        }
        return list;
    }

    public static <T> T fromJson(String json, JavaType type) {

        T object = null;
        try {
            object = mapper.readValue(json, type);
        } catch (Exception e) {
            log.warn("Error while reading object as json:" + json, e);
        }
        return object;
    }

    public static String toJson(Object object) {

        String json = "";
        try {
            json = mapper.writeValueAsString(object);
        } catch (Exception e) {
            log.warn("Error while writing to json string:" + json, e);
        }
        return json;
    }

    public static JsonNode toJsonNode(String jsonString) {

        JsonNode jsonNode = null;
        try {
            jsonNode = mapper.readTree(jsonString);
        } catch (Exception e) {
            log.warn("Error while reading jsonString");
        }
        return jsonNode;
    }
}