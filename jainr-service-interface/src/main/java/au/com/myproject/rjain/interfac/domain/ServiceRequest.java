package au.com.myproject.rjain.interfac.domain;

import au.com.myproject.rjain.interfac.helper.JsonHelper;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;

public class ServiceRequest {

    private String version;
    private String msgId;

    // Start - routing info
    private String serviceId;
    private String resource;
    private String action;
    // End - routing info

    private Object requestData;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @JsonRawValue
    public String getRequestData() {
        return requestData == null ? null : requestData.toString();
    }

    public <T> T getRequestData(Class<T> clazz) {
        return JsonHelper.fromJson(getRequestData(), clazz);
    }

    public <T> T getRequestData(JavaType type) {
        return JsonHelper.fromJson(getRequestData(), type);
    }

    public void setRequestData(JsonNode requestData) {
        this.requestData = requestData;
    }

    public void setRequestData(Object object) {
        if (object != null) {
            this.requestData = JsonHelper.toJson(object);
        }
    }
}